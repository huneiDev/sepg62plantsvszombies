using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TileManager : MonoBehaviour{



	private static TileManager instance;

	public static TileManager Instance{
		get{

			if(instance == null){
				instance = FindObjectOfType<TileManager>();


			}
			return instance;
		}
	}



    [SerializeField]
    private List<Tile> tiles = new List<Tile>();

    [SerializeField]
    private GameObject tilePrefab;

    [SerializeField]
    private Vector2 startPosition;

    [SerializeField]
    private TileCoordinates rows;

    [SerializeField]
    private Vector2 tileOffset;

    public int xBounds;
    public int yBounds;

    private GameObject tileParent;

    private void CreateTile(float x,float y, TileCoordinates coordinates){


        GameObject newTile = (GameObject)GameObject.Instantiate(tilePrefab,new Vector2(x,y),Quaternion.identity);
        Tile tile = newTile.GetComponent<Tile>();
        tile.tileCoordinates = coordinates;
        tile.tileCoordinates.worldPosition = new Vector2(x,y);
        newTile.name = "Tile (" + tile.tileCoordinates.x + ":" + tile.tileCoordinates.y + ")";
        tiles.Add(tile);


    }

    private void GenerateGrid(){


        for(int i=0;i < rows.x;i++){
             
            for(int j=0; j < rows.y;j++){

                float newX = startPosition.x + (i * tileOffset.x);
                float newY = startPosition.y + (j * tileOffset.y);
                CreateTile(newX, newY,new TileCoordinates(i,j));

            }

        }

        xBounds = rows.x;
        yBounds = rows.y;

            

    }

    private void Start(){
        GenerateGrid();
    }

}


