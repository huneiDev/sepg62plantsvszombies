﻿using UnityEngine;
using System.Collections;

public class SunGenerator : MonoBehaviour {

	[SerializeField]
	private GameObject sunPrefab;


	[SerializeField]
	private float shortestTime;

	[SerializeField]

	private float longestTime;


	private void Start(){

		StartCoroutine(SpawnSuns());

	}

	private IEnumerator SpawnSuns(){

		while(true){

			float spawnTime = Random.Range(shortestTime,longestTime);
			Debug.Log("Random Laikas: " + spawnTime);
			yield return new WaitForSeconds(spawnTime);
			float x = Random.Range(-11,5);
			float y= 5.3f;
			GameObject newSun = (GameObject)GameObject.Instantiate(sunPrefab,new Vector2(x,y),Quaternion.identity);
			yield return null;
		}


	}

}
