﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class TileCoordinates{

	public int x;
	public int y;

	public Vector2 worldPosition;

	public TileCoordinates(int x,int y){

		this.x = x;
		this.y = y;

	}


}

public class Tile : MonoBehaviour {


	public TileCoordinates tileCoordinates;

	public int GetX{
		get{
			return tileCoordinates.x;
		}
	}
	public Vector2 GetWorldPosition{
		get{
			return tileCoordinates.worldPosition;
		}
	}
	public int GetY{
		get{
			return tileCoordinates.y;
		}
	}

	
}
