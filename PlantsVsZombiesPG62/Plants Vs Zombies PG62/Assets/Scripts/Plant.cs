﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class PlantStats{

	public int Price;
	public int Health;
	public int Damage;
    public float AttackSpeed;




}

public abstract class Plant : MonoBehaviour {

	public PlantStats plantStats;

    public void TakeDamage(int damage){

        //Jeigu damages mane nuzudys
        if (plantStats.Health - damage <= 0)
        {
            plantStats.Health = 0;
            Destroy(this.gameObject);

        }
        else
        {
            plantStats.Health -= damage;

        }


    }



}
