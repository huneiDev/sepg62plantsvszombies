﻿using UnityEngine;
using System.Collections;

public abstract class Bullet : MonoBehaviour {

	public int Damage;

	public float Speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		transform.Translate(Vector2.right * Speed * Time.deltaTime);


	}

	public abstract void OnHitEnemy(Enemy enemy);
	
	void OnTriggerEnter2D(Collider2D other){

		if(other.gameObject.GetComponent<Enemy>() != null){ // Patikrinam, ar i ka atsitrenkem turi sita scripta.

			Debug.Log("Atsirenkiau");
			OnHitEnemy(other.gameObject.GetComponent<Enemy>());
            Destroy(this.gameObject);
			// Pataikius i zombi ji sustabdyti vietoj.
		}


	}
}
