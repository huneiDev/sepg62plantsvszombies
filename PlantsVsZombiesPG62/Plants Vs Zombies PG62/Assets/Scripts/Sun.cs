using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Sun : MonoBehaviour{

    GameManager gameManager;
    

    private void Start(){

        gameManager = FindObjectOfType<GameManager>();
        
    }

    void OnMouseDown(){

        gameManager.CollectSun(this.gameObject);
        

    }



}