using System;
using System.Collections;
using UnityEngine;


public class DefaultBullet : Bullet
{

    public override void OnHitEnemy(Enemy enemy)
    {

        enemy.TakeDamage(Damage);


    }
}