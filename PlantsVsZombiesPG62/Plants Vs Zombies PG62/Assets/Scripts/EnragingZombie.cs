﻿using UnityEngine;
using System.Collections;

public class EnragingZombie : Enemy {

	
    public bool isEnraged = false;

    public override void TakeDamage(int damage){

        if (zombieStats.Health - damage <= 0)
        {
            Die();


        }
        else
        {
            zombieStats.Health -= damage;
            if (!isEnraged)
            {


                float percantage = zombieStats.FullHealth / zombieStats.Health * 10f;
                if (percantage >= 20f)
                {
                    zombieStats.Speed = zombieStats.Speed * 4f;
                    isEnraged = true;

                }

            }
        }
           


    }




}
