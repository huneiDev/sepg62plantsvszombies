﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PurchasableSunfower : MonoBehaviour {

	public PlantStats plantStats;

	[SerializeField]
	private GameObject prefab;

	private Text priceText;

	Button button;
	Image renderer;

	public bool isPreparingPlant = false; // Ar jau ruosiames padeti ant kazkurio tile.

	private void Spawn(Vector2 pos,Tile tile){

		GameObject newPlant = (GameObject)GameObject.Instantiate(prefab,pos,Quaternion.identity);
	
	}

	public void OnSuccesfulPlant(Vector2 position, Tile tile){

		Spawn(position,tile);
		GameManager.Instance.CurrentSunflowers -= plantStats.Price;
		GameManager.Instance.currentSelectedSunflower = null;

	}
	public void Select(){
		if(GameManager.Instance.CurrentSunflowers >=  plantStats.Price){
			GameManager.Instance.currentSelectedSunflower = this;
		}
	}

	 private void initializeVariables(){ // Nutatyti buttonu, imagu reiksmes.

	 	if(transform.FindChild("Price") != null){
			 priceText = transform.FindChild("Price").GetComponent<Text>();
		 }
		 button = GetComponent<Button>();
		 renderer = GetComponent<Image>();
		 priceText.text = plantStats.Price.ToString();

	 }

	 private void Start(){
		 initializeVariables();
	 }



}
