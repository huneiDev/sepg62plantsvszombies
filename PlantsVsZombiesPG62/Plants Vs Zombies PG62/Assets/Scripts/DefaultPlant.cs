using System.Collections;
using UnityEngine;


public class DefaultPlant : Plant{

    [SerializeField]
    private GameObject bullet;
    
    [SerializeField]
    private float bulletSpeed;


    private void Start(){

       StartCoroutine(Shoot());

    }

    private IEnumerator Shoot(){

        while(true){
            yield return new WaitForSeconds(plantStats.AttackSpeed);
            GameObject newbullet = (GameObject)Instantiate(bullet,transform.position,Quaternion.identity);
        
        
        }
    }




}