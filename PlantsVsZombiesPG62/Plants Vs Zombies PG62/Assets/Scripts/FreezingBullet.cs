using System;
using System.Collections;
using UnityEngine;


public class FreezingBullet : Bullet
{

    public float freezeTime;

    private IEnumerator Freeze(Enemy enemy){

        float startSpeed = enemy.zombieStats.Speed;
        enemy.zombieStats.Speed = 0;
        yield return new WaitForSeconds(freezeTime);
        enemy.zombieStats.Speed = startSpeed;
    }

    public override void OnHitEnemy(Enemy enemy)
    {
        StartCoroutine(Freeze(enemy));
        enemy.TakeDamage(Damage);


    }
}