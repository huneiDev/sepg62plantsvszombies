﻿using UnityEngine;
using System.Collections;



[System.Serializable]
public class ZombieStats{

	public int Health;
	public int Damage;
    public int FullHealth = 100;
	public float Speed;
	public float AttackSpeed;

	public ZombieStats(){

		Health = 100;
		Damage = 1;
		Speed = 0.1f;
		AttackSpeed = 1f;


	}

	public ZombieStats(int Health, int Damage, float Speed,float AttackSpeed){

		this.Health = Health;
		this.Damage = Damage;
		this.Speed = Speed;
		this.AttackSpeed = AttackSpeed;

	}


}


public abstract class Enemy : MonoBehaviour {

	public ZombieStats zombieStats;
    public Plant currentPlant = null;

	private void Start(){

		StartCoroutine(Move());

	}

	//Judejimas i kaire puse
	private IEnumerator Move(){

		while(true){

			transform.Translate(Vector2.left * GetSpeed * Time.deltaTime);
			yield return null;
			
		}

	}

    private void OnTriggerEnter2D(Collider2D other){

        // Turi sita scripta
        if (other.gameObject.GetComponent<Plant>() != null)
        {
            currentPlant = other.gameObject.GetComponent<Plant>();
            StartCoroutine(StartEating());
        }


    }

    private IEnumerator StartEating(){
        float previousSpeed = zombieStats.Speed;
        zombieStats.Speed = 0;
        while (currentPlant != null && currentPlant.plantStats.Health > 0)
        {
            currentPlant.TakeDamage(zombieStats.Damage);
            yield return new WaitForSeconds(zombieStats.AttackSpeed);
        }
        //Kai suvalgom
        zombieStats.Speed = previousSpeed;


    }

    public virtual void TakeDamage(int damage){

        if (zombieStats.Health - damage <= 0)
        {
            zombieStats.Health = 0;
            Die();
        }
        else
        {
            zombieStats.Health -= damage;

        }


	}

	public void Die(){
        Destroy(gameObject);
		Debug.Log("Miriau");
	}

	public int GetHealth{

		get{

			return zombieStats.Health;

		}

	}
	public int GetDamage{
		get{

			return zombieStats.Damage;

		}
	}

	public float GetSpeed{
		get{
			return zombieStats.Speed;
		}
	}

	public float GetAttackSpeed{
		get{
			return zombieStats.AttackSpeed;
		}
	}



}
