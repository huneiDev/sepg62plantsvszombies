﻿using UnityEngine;
using System.Collections;

public class Sunflower : Plant {

	[SerializeField]
	private GameObject sunPrefab;


	private IEnumerator GenerateSunflowers(){

			while(true){
				yield return new WaitForSeconds(5f);
				GameObject newSun = (GameObject)Instantiate(sunPrefab,new Vector3(transform.position.x,transform.position.y,-9),Quaternion.identity);
				newSun.GetComponent<Rigidbody2D>().gravityScale = 0;
			}
		

	}

	void Start(){

		StartCoroutine(GenerateSunflowers());

	}

	

}
