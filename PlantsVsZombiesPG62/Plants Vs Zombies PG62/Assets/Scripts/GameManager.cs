﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {



	private static GameManager instance;

	public static GameManager Instance{
		get{

			if(instance == null){
				instance = FindObjectOfType<GameManager>();


			}
			return instance;
		}
	}

	public PurchasableSunfower currentSelectedSunflower;

	public int currentSunflowers;



    public int CurrentSunflowers{

        get
        {
            return currentSunflowers;
        }

        set
        {
            currentSunflowers = value;
            UpdateSunflowersText();
        }


    }

	[SerializeField]
	 private Text sunflowerText;

	public int sunCollectReward;

	public void CollectSun(GameObject sun){

        CurrentSunflowers += sunCollectReward;
		Destroy(sun);

	}

    private void UpdateSunflowersText(){

        sunflowerText.text = CurrentSunflowers.ToString();

    }

	private void ProcessRay(RaycastHit2D hit){ //Apdoroti paspaudima

		if(hit.transform.GetComponent<Tile>() != null){

			currentSelectedSunflower.OnSuccesfulPlant(hit.transform.position,hit.transform.GetComponent<Tile>());

		}

	}

	private void DoRay(){ // Tikrinti paspaudima

		Camera camera = Camera.main;
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast(ray.origin,ray.direction);

		if(hit.transform){
			ProcessRay(hit);
		}

	}

	private void Update(){
		if(Input.GetMouseButtonDown(0)){
			DoRay();
		}
	}


}
